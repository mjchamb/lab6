/**************************
 *Morgan Chambers
 *CPSC 1021, 003, F20
 *mjchamb@clemson.edu
 *Elliot
 **************************/

#include <iostream>

#include <cmath>

using namespace std;

class point

{

private:

double x,y;

public:

point();

double getX();

double getY();

void shift(double x_amount, double y_amount);

void rotate90();

bool operator==(point &other);

};

// default constructor to set x and y to 0

point::point() : x(0), y(0)

{}

// return the x-coordinate

double point::getX()

{

return x;

}

// return the y-coordinate

double point::getY()

{

return y;

}

// shift x and y by x_amount and y_amount

void point::shift(double x_amount, double y_amount)

{

x += x_amount;

y += y_amount;

}

// rotate the point by 90 degrees in counterclockwise

void point::rotate90()

{

double temp = x;

x = -y;

y = temp;

}

// returns true if both points are equal else false

bool point::operator ==(point &other)

{

const double EPSILON = 1e-5;

return((fabs(getX()-other.getX()) < EPSILON) && (fabs(getY()-other.getY()) < EPSILON) );

}

//end of point class

// non-member function returns the number of rotations needed to move p into the upper-left quadrant (where x<0, y>0)

int rotations_needed(point p)

{

int count=0;

// loop continues until point p is in third quadrant

while(!((p.getX() <= 0) && (p.getY() >= 0)))

{

count++;

p.rotate90();

}

return count;

}

// non-member function rotate p to move it to the upper-left quadrant

void rotate(point &p)

{

// loop continues until point p is in third quadrant

while(!((p.getX() <= 0) && (p.getY() >= 0)))

{

p.rotate90();

}

}
